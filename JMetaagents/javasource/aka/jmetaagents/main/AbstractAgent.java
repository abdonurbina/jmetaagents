package aka.jmetaagents.main;

import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

/**
 * @author Charlotte
 */
public abstract class AbstractAgent {

    private static Logger LOGGER = null;

    /**
     * Invoke method to read value and return requested result
     *
     * @param className
     * @param jsonString
     * @return result
     */
    @SuppressWarnings("unchecked")
    @Nullable
    protected final <T, @Nullable S> S readValue(final Class<T> className, final String jsonString) {
        S result = null;
        try {
            result = AccessController.doPrivileged((PrivilegedExceptionAction<S>) () -> {
                final Method method = className.getMethod("readValue", String.class);
                return (S) method.invoke(null, jsonString);
            });
        } catch (final PrivilegedActionException e) {
            getLogger().logp(Level.SEVERE, "AbstractAgent", "readValue", e.getMessage(), e);
        }

        return result;
    }

    /**
     * Get logger
     *
     * @return logger
     */
    @NonNull
    protected final Logger getLogger() {
        Logger result = LOGGER;
        if (result == null) {
            final String packageName = AbstractAgent.class.getClass().getName();
            result = LOGGER = Logger.getLogger(packageName);
            assert result != null;
        }

        return result;
    }
}
