package aka.jmetaagents.main.constants;

/**
 * Movie constants
 */
public final class MovieConstants {

    /**
     * Certification general : {@value}
     */
    public static final String MOVIE_CERTIFICATION_GENERAL = "mpaa_general";
    /**
     * Certification NC17 : {@value}
     */
    public static final String MOVIE_CERTIFICATION_NC17 = "mpaa_nc17";
    /**
     * Certification norated : {@value}
     */
    public static final String MOVIE_CERTIFICATION_NORATED = "mpaa_notrated";
    /**
     * Certification PG : {@value}
     */
    public static final String MOVIE_CERTIFICATION_PG = "mpaa_pg";
    /**
     * Certification PG-13 : {@value}
     */
    public static final String MOVIE_CERTIFICATION_PG13 = "mpaa_pg13";
    /**
     * Certification restricted : {@value}
     */
    public static final String MOVIE_CERTIFICATION_RESTRICTED = "mpaa_restricted";

    private MovieConstants() {
        // singleton
    }

}
