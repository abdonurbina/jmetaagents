package aka.jmetaagents.main.constants;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;

/**
 * List of constants for the differents helpers.
 *
 * @author Charlotte
 *
 */
public final class MetaagentConstants {

    /**
     * The movie DB API key.
     */
    @NonNull
    public static final String THE_MOVIE_DB_API_KEY = "807efd13f0a812f03dba245d261e2b65";
    /**
     * The movie DB API key.
     */
    @NonNull
    public static final String ALLOCINE_API_KEY = "YW5kcm9pZC12M3M";
    /**
     * The rotten tomatoes API key.
     */
    @NonNull
    public static final String ROTTENTOMATOES_API_KEY = "u8ntuxdqydqs993p2nw8pufe";

    /**
     * Mapping between TheMovieDb certification string and {@link MovieConstants} certification constants.
     *
     * Map<String, String> corresponding to TheMovieDb certification as key with media certification as value
     */
    @NonNull
    public static final Map<@NonNull String, String> MOVIEDB_MOVIE_CERTIFICATION_MAP;

    /**
     * Screenplay tag for The movie DB.
     */
    @NonNull
    public static final String THE_MOVIE_DB_JOB_SCREENPLAY = "Screenplay";
    /**
     * Director tag for The movie DB.
     */
    @NonNull
    public static final String THE_MOVIE_DB_JOB_DIRECTOR = "Director";
    /**
     * release tag for rotten tomatoes.
     */
    @NonNull
    public static final String ROTTENTOMATOES_MOVIE_RELEASEDATE = "theater";
    /**
     * original tag for rotten tomatoes.
     */
    @NonNull
    public static final String ROTTENTOMATOES_MOVIE_POSTER = "original";

    // ---------------------------------- INITIALISATION ----------------------------------------//

    static {
        MOVIEDB_MOVIE_CERTIFICATION_MAP = new HashMap<>();
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("G", "mpaa_general");
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("NC17", "mpaa_nc17");
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("", "mpaa_notrated");
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("PG", "mpaa_pg");
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("PG-13", "mpaa_pg13");
        MOVIEDB_MOVIE_CERTIFICATION_MAP.put("R", "mpaa_restricted");
    }

    private MetaagentConstants() {
        // singleton
    }
}
