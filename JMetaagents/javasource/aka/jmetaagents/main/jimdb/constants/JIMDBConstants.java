package aka.jmetaagents.main.jimdb.constants;

import org.eclipse.jdt.annotation.NonNull;

/**
 * JIMDB related constants.
 *
 * @author Cha
 */
public final class JIMDBConstants {

    /**
     * Movie URL.
     */
    @NonNull
    public static final String MOVIE_URL = "http://app.imdb.com/title/maindetails?";
    /**
     * Search URL.
     */
    @NonNull
    public static final String SEARCH_MOVIE_URL = "http://app.imdb.com/find?";

    /**
     * API.
     */
    @NonNull
    public static final String API = "api";
    /**
     * API value.
     */
    @NonNull
    public static final String API_VALUE = "v1";

    /**
     * API ID.
     */
    @NonNull
    public static final String API_ID = "appid";
    /**
     * API ID value.
     */
    @NonNull
    public static final String API_ID_VALUE = "iphone2_0";

    /**
     * Sig.
     */
    @NonNull
    public static final String SIG = "sig";
    /**
     * Sig value.
     */
    @NonNull
    public static final String SIG_VALUE = "app1_1-f6gf8b542aa3cc4d16091adf111e99b1358387635";

    /**
     * Device.
     */
    @NonNull
    public static final String DEVICE = "device";
    /**
     * Device value.
     */
    @NonNull
    public static final String DEVICE_VALUE = "a1e16abac75342d4a1b42a64a4c3bf195g530a1d";

    /**
     * TConst.
     */
    @NonNull
    public static final String TCONST = "tconst";
    /**
     * Query.
     */
    @NonNull
    public static final String QUERY = "q";
    /**
     * Count.
     */
    @NonNull
    public static final String COUNT = "count";
    /**
     * Page.
     */
    @NonNull
    public static final String PAGE = "page";

    private JIMDBConstants() {
        // singleton
    }
}
